# syntax=docker/dockerfile:1
FROM ruby:2.7
WORKDIR /myapp
COPY . .
EXPOSE 3000

# Configure the main process to run when running the image
CMD ["ruby", "details.rb", "3000"]