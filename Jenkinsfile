// Define variables
def scmVars

// Start Pipeline
pipeline {
    
    agent{
        kubernetes{
            yaml """
        apiVersion: v1
        kind: Pod
        spec:
            containers:
            - name: docker
              image: docker:20.10.3-dind
              command:
              - dockerd
              - --host=unix:///var/run/docker.sock
              - --host=tcp://0.0.0.0:2375
              - --storage-driver=overlay2
              tty: true
              securityContext:
                privileged: true
            - name: helm
              image: lachlanevenson/k8s-helm:v3.5.0
              command:
              - cat
              tty: true
            - name: skan
              image: alcide/skan:v0.9.0-debug
              command:
              - cat
              tty: true
            - name: java-ruby
              image: jruby:9.2-jdk11
              command:
              - cat
              tty: true
              volumeMounts:
              - mountPath: /home/jenkins/dependency-check-data
                name: dependency-check-data
            volumes:
            - name: dependency-check-data
              hostPath:
                path: /tmp/dependency-check-data
        """
        }//End kubernetes
    }//End agent

    environment {
        ENV_NAME = "${BRANCH_NAME == "master" ? "uat" : "${BRANCH_NAME}"}"
        SCANNER_HOME = tool 'sonarqube-scanner'
        PROJECT_KEY = 'jj-book-info-detail'
        PROJECT_NAME = 'jj-book-info-detail'
        ORGENIZATION = 'jj-book-info'
    }//End environment
    
    //Start Pipeline
    stages {
        // ***** Stage Clone *****
        stage('Clone details source code') {
            steps {
                //Run in Jenkins Slave container
                container('jnlp'){
                    script{ 
                        scmVars = git branch: "${BRANCH_NAME}",
                                  credentialsId: 'jj-bookinfo-git-deploy-key-details',
                                  url: 'git@gitlab.com:bookinfo/details.git'
                    }//End script
                }//End container
            }//End step
        }//End stage

        stage('sKan') {
            steps {
                container('helm') {
                    script {
                        // Generate k8s-manifest-deploy.yaml for scanning
                        sh "helm template -f k8s/helm-values/values-bookinfo-${ENV_NAME}-details.yaml \
                            --set extraEnv.COMMIT_ID=${scmVars.GIT_COMMIT} \
                            --namespace bookinfo-${ENV_NAME} details-${ENV_NAME} k8s/helm \
                            > k8s-manifest-deploy.yaml"
                    }
                }
                container('skan') {
                    script {
                        // Scanning with sKan
                        sh "/skan manifest -f k8s-manifest-deploy.yaml"
                        // Keep report as artifacts
                        archiveArtifacts artifacts: 'skan-result.html'
                        sh "rm k8s-manifest-deploy.yaml"
                    }
                }
            }
        }

            // ***** Stage Sonarqube *****
        stage('Sonarqube Scanner') {
            steps {
                container('java-ruby'){
                    script {
                        // Authentiocation with https://sonarcloud.io
                        withSonarQubeEnv('Sonarcloud') {
                            // Run Sonar Scanner
                            sh '''${SCANNER_HOME}/bin/sonar-scanner \
                            -D sonar.organization=${ORGENIZATION} \
                            -D sonar.projectKey=${PROJECT_KEY} \
                            -D sonar.projectName=${PROJECT_NAME} \
                            -D sonar.projectVersion=${BRANCH_NAME}-${BUILD_NUMBER} \
                            -D sonar.sources=details.rb
                            '''
                        }//End withSonarQubeEnv

                        // Run Quality Gate
                        timeout(time: 1, unit: 'MINUTES') { 
                            def qg = waitForQualityGate()
                            if (qg.status != 'OK') {
                                error "Pipeline aborted due to quality gate failure: ${qg.status}"
                            }
                        } // End Timeout
                    } // End script
                } // End container
            } // End steps
        } // End stage

        stage('OWASP Dependency Check') {
            steps {
                container('java-ruby') {
                    script {

                        // Start OWASP Dependency Check
                        dependencyCheck(
                            additionalArguments: "--data /home/jenkins/dependency-check-data --out dependency-check-report.xml",
                            odcInstallation: "dependency-check"
                        )

                        // Publish report to Jenkins
                        dependencyCheckPublisher(
                            pattern: 'dependency-check-report.xml'
                        )

                    } // End script
                } // End container
            } // End steps
    } // End stage
        
        stage('Build details Docker Image and push'){
            steps{
                container('docker'){
                    script{
                        docker.withRegistry('','registry-bookinfo'){
                            docker.build('jinjustin/bookinfo-detail:${ENV_NAME}').push()
                        }//End docker.withRegistry
                    }//End script
                }//End container
            }//End steps
        }//End stage

        // ***** Stage Anchore *****
        stage('Anchore Engine') {
            steps {
                container('jnlp') {
                    script {
                        // dend Docker Image to Anchore Analyzer
                        writeFile file: 'anchore_images' , text: "jinjustin/bookinfo-detail:${ENV_NAME}"
                        anchore name: 'anchore_images' , bailOnFail: false
                    } // End script
                } // End container
            } // End steps
        } // End stage
        
        stage('Deploy details with Helm Chart'){
            steps{
                container('helm'){
                    script{
                        withKubeConfig([credentialsId: 'gke-kubeconfig']){
                            
                            sh "helm upgrade -i -f k8s/helm-values/values-bookinfo-${ENV_NAME}-details.yaml --wait --set extraEnv.COMMIT_ID=${scmVars.GIT_COMMIT} --namespace bookinfo-${ENV_NAME} bookinfo-${ENV_NAME}-details k8s/helm"
                            
                        }//End withKubeConfig
                    }//End script
                }//End container
            }//End steps
        }//End stage
        
    }//End stages
}//End Pipeline